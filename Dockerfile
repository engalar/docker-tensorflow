# This file is a template, and might need editing before it works on your project.
FROM tensorflow/tensorflow:1.12.0-gpu-py3

RUN apt-get update && apt-get install -y \
  libsm6 \
  libxext6 \
  libxrender-dev \
 && rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install \
        Flask==0.10.1 \
        matplotlib==1.5.1 \
        scipy==0.19.0 \
        plumbum==1.6.2 \
        numpy==1.12.1 \
        ipython==6.1.0 \
        Pillow==4.2.1 \
        Shapely \
        opencv-python

WORKDIR /app

# TensorBoard
EXPOSE 6006
# IPython
EXPOSE 8888

CMD ["/run_jupyter.sh", "--allow-root"]